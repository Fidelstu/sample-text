
<?php
    include('../Config/connect.php'); 
    $sql	= 'select * from sample';
    $query	= mysqli_query($db_link,$sql);
?>

<div style="margin-left:150px; margin-right: 150px">
    <h2><strong><p align="center">List of sample text</p></strong></h2>
    <table class="table table-striped table-bordered">
        <thead class="thead-dark">
        <tr>
            <th scope="col" colspan="2" >
                <div class="bc" id="addList" title="Add new"><i class="fas fa-plus"></i> Add</div>
            </th>
            <th class="text-right" scope="col">ID</th>
            <th scope="col">Text</th>
            <th scope="col">Active</th>
            
        </tr>
        </thead>   
        
        <?php
            while($data	= mysqli_fetch_array($query)){ 
        ?>
            <tr>
                <td width="45px">
                    <div class="bc editRowList" title="Edit">  <i class="far fa-edit"></i>  </div> 
                <td width="45px">
                    <div class="bc deleteRowList" title="Delete"><i class="fas fa-trash-alt"></i></div>
                </td>
                <td class="idRow text-right" width="50px" ><?php echo $data['id']; ?></td>
                <td class="textRow"><?php echo $data['text']; ?></td>
                <td class="activeRow" width="200px"> 
                    <?php if($data['active']=="1"){?>                         
                        <i class="fas fa-check" title="Active"></i>                       
                        <?php }else { ?>
                        <div class="bc setActive">
                        <i class="far fa-check-square"></i>    set for active
                        </div>
                    <?php } ?>
                    
                </td>
                
            </tr>
        <?php
        }
        ?>
    </table>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Form Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">        
        <button type="button" class="btn btn-primary" id="saveForm"><i class="far fa-hdd"></i> Save</button>
      </div>
    </div>
  </div>
</div>
   