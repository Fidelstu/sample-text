<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        
        <link rel="stylesheet" type="text/css" href="./Static/CSS/main.css">        
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.0/css/all.css" integrity="sha384-Mmxa0mLqhmOeaE8vgOSbKacftZcsNYDjQzuCOm6D02luYSzBG8vpaOykv9lFQ51Y" crossorigin="anonymous">

        <script src="./Static/JS/jquery-3.3.1.min.js"> </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <title>Sample Text</title>

    </head>
    <body>   
        <div class="collapse" id="navbarToggleExternalContent">
            <nav class="navbar navbar-dark" style="background-color: #e3f2fd;">
                <div class="bc nav-item" id="homeClick"><i class="fas fa-home"></i> Home</div>
                <div class="bc nav-item" id="manageClick"><i class="fas fa-cog"></i> Manage</div>  
            </nav>
        </div>
        <nav class="navbar navbar-dark" style="background-color: #3B5998;>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-chevron-circle-down"></i>
            </button>
        </nav>
        

        <div id="index"> 
              
        </div>  

        <script src="./Static/JS/controller.js"></script>
    </body>
</html>