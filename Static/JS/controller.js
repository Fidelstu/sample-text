$.ajaxSetup({
  cache: true
});

$(document).ready(function(){
    const qs = qsFn();
    let location= window.location.origin + window.location.pathname;

    if(!qs["P"]){
      $("#index").load('./Views/Display.php');      
    }else{
      $("#index").load('./Views/List.php',listConLoad);
    }

    $("#manageClick").click(function(){
     location += "?p=1";
      window.open(location,"_self");
    });

    $("#homeClick").click(function(){
      window.open(location,"_self");
    });    


  });

  function qsFn(){
    const result = location.search.match(/[\?\&][^\?\&]+=[^\?\&]+/g);
    let qs = {};
    for(var i = 0; result && i < result.length; i++){
      var str = result[i].substring(1),
        keyStr = str.split('=')[0].toUpperCase(),
        valueStr = str.split('=')[1];
       qs[keyStr] = valueStr;
     }
     return qs;
  }

 function listConLoad(){
  $.getScript("./Static/JS/listController.js")
 }