$(document).ready(function(){
  const qs = qsFn();
  const location= window.location.href;
    
    $("#addList").click(function(){
      debugger
      $("#myModal").modal();
      loadForm();
    });

    $(".editRowList").click(function(){
      const trElement = $(this).closest("tr");

      const row = {
        id: trElement.find(".idRow").text().trim()
        ,text: trElement.find(".textRow").text().trim()
        ,active: trElement.find(".activeRow").text().trim()
      }
      debugger
      $("#myModal").modal("show");
      loadForm(row);
    });

    $(".deleteRowList").click(function(){
      debugger
      const trElement = $(this).closest("tr");
      deleteRowList({
        id:trElement.find(".idRow").text().trim()
      })
      
    });

    $(".setActive").click(function(){      
      const trElement = $(this).closest("tr");
      const row = {
        id: trElement.find(".idRow").text().trim()
        ,location: location
      }
      setActive(row);
    });

    $("#saveForm").click(function(){      
      const text = $("#myModal #inputTextForm").val().trim();
      const key =$("#myModal #inputTextForm").data("key");
      
      saveForm({
        text: text
        ,id: key
      })      
      
    });


  });

  function loadForm(obj){
    $(".modal-body").load('./Views/Form.php',obj);  
  }

  function setActive(obj){
    $.post('./Act/setActiveAct.php',obj)
        .done(function( data ) {
          $("#index").load('./Views/List.php',listConLoad);
        });     
  }

  function saveForm(obj){
    $.post( './Act/act.php',obj)
        .done(function( data ) {
          $("#myModal").modal("hide");          
          $("#myModal").on('hidden.bs.modal', function(){            
            $("#index").load('./Views/List.php',listConLoad);
          });
        });    
  }

  function deleteRowList(obj){
    $.post( './Act/deleteAct.php',obj)
        .done(function( data ) {
          $("#index").load('./Views/List.php',listConLoad);
        });    
  }

  function listConLoad(){
    $.getScript("./Static/JS/listController.js")
   }
  
   function formConLoad(){
    $.getScript("./Static/JS/formController.js")
   }
   
 